using Player;

public class BoardModel
{
    public Piece[,] Board { get; private set; }
    public BoardCoord WhiteKingPosition { get; set; }
    public BoardCoord BlackKingPosition { get; set; }

    public BoardModel()
    {
        Board = new Piece[8, 8];
    }

    private BoardModel(Piece[,] model)
    {
        Board = model;
        UpdateKingsPosition();
    }

    public void SetStartingPosition()
    {
        Board = FenUtils.ReadPosition(FenUtils.StartPos);
        WhiteKingPosition = new BoardCoord('e', 1);
        BlackKingPosition = new BoardCoord('e', 8);
    }
    
    public Piece GetPiece(BoardCoord coord)
    {
        return Board[coord.GetXIndex(), coord.GetYIndex()];
    }

    public void SetPiece(Piece piece, BoardCoord coord)
    {
        Board[coord.GetXIndex(), coord.GetYIndex()] = piece;
    }
    
    public BoardModel DeepCopy()
    {
        Piece[,] copy = new Piece[8, 8];

        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                copy[i, j] = Board[i, j];
            }
        }

        return new BoardModel(copy);
    }
    
    public void UpdateKingsPosition()
    {
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                if (Board[i, j] == null) continue;
                if (Board[i, j].Type != Piece.PieceType.King) continue;
                if (Board[i, j].Side == Side.White) WhiteKingPosition = new BoardCoord(i, j);
                if (Board[i, j].Side == Side.Black) BlackKingPosition = new BoardCoord(i, j);
            }
        }
    }
}
