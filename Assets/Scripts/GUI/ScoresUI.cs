﻿using System;
using Assets.Scripts;
using Player;
using UnityEngine;
using UnityEngine.UI;

namespace GUI
{
    public class ScoresUI : MonoBehaviour
    {
        public RectTransform whitesPanel;
        public RectTransform blacksPanel;
        public Text whiteScore;
        public Text blackScore;

        private int _whiteScore;
        private int _blackScore;

        public float iconSize;
        
        [Header("Pieces' sprites")]
        public Sprite whitePawn;
        public Sprite blackPawn;
        
        public Sprite whiteBishop;
        public Sprite blackBishop;
        
        public Sprite whiteKnight;
        public Sprite blackKnight;
        
        public Sprite whiteRook;
        public Sprite blackRook;
        
        public Sprite whiteQueen;
        public Sprite blackQueen;

        void Awake()
        {
            ResetScore();
        }

        Sprite GetSprite(Piece.PieceType type, Side side)
        {
            switch (type)
            {
                case Piece.PieceType.Pawn:
                    return side.Equals(Side.White) ? whitePawn : blackPawn;
                case Piece.PieceType.Bishop:
                    return side.Equals(Side.White) ? whiteBishop : blackBishop;
                case Piece.PieceType.Knight:
                    return side.Equals(Side.White) ? whiteKnight : blackKnight;
                case Piece.PieceType.Rook:
                    return side.Equals(Side.White) ? whiteRook : blackRook;
                case Piece.PieceType.Queen:
                    return side.Equals(Side.White) ? whiteQueen : blackQueen;
                default:
                    throw new Exception();
            }
        }

        public void ResetScore()
        {
            _whiteScore = 0;
            _blackScore = 0;
        }

        public void AddPieceIcon(Piece.PieceType type, Side side)
        {
            GameObject obj = new GameObject();
            Image icon = obj.AddComponent<Image>();
            icon.sprite = GetSprite(type, side);
            RectTransform rt = obj.GetComponent<RectTransform>(); 
            rt.sizeDelta = new Vector2(iconSize, iconSize);

            rt.SetParent(side.Equals(Side.Black) ? whitesPanel.transform : blacksPanel.transform);

            obj.SetActive(true);
        }

        public void AddEatenPiece(Piece piece)
        {
            AddPieceIcon(piece.Type, piece.Side);

            if (piece.Side.Equals(Side.Black))
            {
                _whiteScore += piece.Value;
            }
            else
            {
                _blackScore += piece.Value;
            }

            whiteScore.text = Mathf.Clamp(_whiteScore - _blackScore, 0, 100).ToString();
            blackScore.text = Mathf.Clamp(_blackScore - _whiteScore, 0, 100).ToString();
        }
    }
}
