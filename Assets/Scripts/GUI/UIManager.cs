﻿using UnityEngine;

namespace GUI
{
    public class UIManager : MonoBehaviour
    {

        public GameObject gameOverScreen;

        public void ShowGameOverScreen()
        {
            gameOverScreen.SetActive(true);
        }

        public void HideGameOverScreen()
        {
            gameOverScreen.SetActive(false);
        }
    }
}
