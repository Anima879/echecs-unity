﻿using System.Collections;
using OutlineEffect;
using Player;
using UnityEngine;

[RequireComponent(typeof(Outline))]
public class Piece3D : MonoBehaviour
{
    private static int SELECTION_OUTLINE = 2;

    public Side side;
    public BoardCoord Coord { get; set; }
    public float speed = 1f;
    public bool isMoving;


    private Outline _outliner;

    public bool mouseEvent;

    protected void Awake()
    {
        _outliner = GetComponent<Outline>();
        mouseEvent = true;
        isMoving = false;
    }

    // Start is called before the first frame update
    protected void Start()
    {
        _outliner.eraseRenderer = true;
    }

    protected void OnMouseExit()
    {
        if (mouseEvent) _outliner.eraseRenderer = true;
    }

    public void ShowSelectionOutline()
    {
        _outliner.eraseRenderer = false;
        _outliner.color = SELECTION_OUTLINE;
    }

    public void HideOutline()
    {
        _outliner.eraseRenderer = true;
    }

    public void Move(Vector3 position)
    {
        isMoving = true;
        StartCoroutine(MoveAnimation(new Vector3(position.x, .05f, position.z)));
    }

    protected virtual IEnumerator MoveAnimation(Vector3 targetPosition)
    {
        Vector3 startPos = transform.position;
        float distance = Vector3.Distance(startPos, targetPosition);
        float progress = 0.0f;

        while (progress / distance < 1)
        {
            float t = progress / distance;
            progress += speed * Time.deltaTime;
            transform.position = Vector3.Lerp(startPos, targetPosition, t);

            yield return null;
        }

        transform.position = targetPosition;
        isMoving = false;
    }

    private bool Equals(Piece3D other)
    {
        return base.Equals(other);
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != this.GetType()) return false;
        return Equals((Piece3D) obj);
    }

    public override int GetHashCode()
    {
        unchecked
        {
            int hashCode = base.GetHashCode();
            hashCode = (hashCode * 397) ^ (int)side;
            hashCode = (hashCode * 397) ^ Coord.GetHashCode();
            return hashCode;
        }
    }
}