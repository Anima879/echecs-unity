﻿using System;
using System.Collections;
using System.Collections.Generic;
using GUI;
using Player;
using Player.IA;
using UnityEngine;

public class GameMaster : MonoBehaviour
{
    private static readonly LayerMask PieceLayerMask = 1 << 9;
    
    public Board boardManager;

    public bool playAgainstStockfish;

    private AbstractPlayer _whites, _blacks;
    public Transform whitePlayer, blackPlayer;
    public ChessEngine Engine { get; private set; }

    private Camera _mainCamera;

    // UI
    public ScoresUI scoresUi;
    public UIManager uiManager;
    
    // Finite state machine
    private GameState _state;
    
    private IEnumerator _stateRoutine;
    private IEnumerator _finiteStateMachine;

    private enum GameState
    {
        Init,
        WhiteToMove,
        BlackToMove,
        WhiteIsCheckmate,
        BlackIsCheckMate,
        Stalemate,
        Promotion,
        Exit
    }

    private void Awake()
    {
        whitePlayer.gameObject.name = "White";
        _whites = whitePlayer.gameObject.AddComponent<HumanPlayer>();
        _whites.PlayerSide = Side.White;

        blackPlayer.gameObject.name = "Black";

        _blacks = playAgainstStockfish
            ? (AbstractPlayer) blackPlayer.gameObject.AddComponent<IAPlayer>()
            : blackPlayer.gameObject.AddComponent<HumanPlayer>();

        _blacks.PlayerSide = Side.Black;
        _finiteStateMachine = FiniteStateMachine(GameState.Init);
    }

    // Start is called before the first frame update
    private void Start()
    {
        _mainCamera = Camera.main;
        // 3D Board initialization
        
        // ChessEngine.DisplayBoardDebug(_chess.GetCurrentBoard());
        StartCoroutine(DebugRoutine());

        
        StartCoroutine(_finiteStateMachine);
    }

    private void LateUpdate()
    {
        // playerTurn = Engine.GetPlayerTurn();
        // boardManager.playerTurn = playerTurn;
    }
    
    private IEnumerator FiniteStateMachine(GameState state)
    {
        _state = state;
        while (_state != GameState.Exit)
        {
            switch (_state)
            {
                case GameState.Init:
                    _stateRoutine = Init();
                    break;
                case GameState.WhiteToMove:
                    _stateRoutine = Move(_state, Side.White);
                    break;
                case GameState.BlackToMove:
                    _stateRoutine = Move(_state, Side.Black);
                    break;
                case GameState.WhiteIsCheckmate:
                    break;
                case GameState.BlackIsCheckMate:
                    break;
                case GameState.Stalemate:
                    break;
                case GameState.Promotion:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
                
            yield return StartCoroutine(_stateRoutine);
        }
    }
    
    private IEnumerator Init()
    {
        // Things to do when entering the state.
        boardManager.AssignPlayer(_whites, _blacks);
        _whites.SetGameMaster(this);
        _blacks.SetGameMaster(this);
        // Chess game initialization
        StandardGameInitialization();
        
        Engine.OnPieceDeath += scoresUi.AddEatenPiece;
        Engine.OnCheckmate += GameOver;
        Engine.OnPromotion += boardManager.Promote;
        
        _state = GameState.WhiteToMove;
        yield return null;
    }
    
    private IEnumerator Move(GameState state, Side side)
    {
        AbstractPlayer player = side == Side.White ? _whites : _blacks;
        AbstractPlayer opponent = side == Side.White ? _blacks : _whites;

        player.PlayerState = AbstractPlayer.State.Playing;
        opponent.PlayerState = AbstractPlayer.State.Waiting;
        while (_state == state)
        {
            if (player.PlayerState == AbstractPlayer.State.Waiting)
            {
                _state = side == Side.White ? GameState.BlackToMove : GameState.WhiteToMove;
            }
            yield return null;
        }
    }
    
    private IEnumerator Checkmate(GameState state)
    {
        while (_state == state)
        {
            yield return null;
        }
    }
    
    private IEnumerator Stalemate(GameState state)
    {
        while (_state == state)
        {
            yield return null;
        }
    }
    
    private IEnumerator Promotion(GameState state)
    {
        while (_state == state)
        {
            yield return null;
        }
    }

    public bool PlayMove(BoardCoord start, BoardCoord destination)
    {
        if (Engine.PlayMove(start, destination))
        {
            boardManager.MovePiece(start, destination);
            ResetBoardTiles();
            return true;
        }
        
        Debug.LogError("Move " + start + "-" + destination + " cannot be played");
        return false;
    }

    public List<BoardCoord> ShowAvailableMoves(Piece3D piece)
    {
        List<BoardCoord> moves = Engine.GetLegalMoves(piece.Coord);
        boardManager.ShowMovementTiles(moves);
        return moves;
    }

    public void ResetBoardTiles()
    {
        boardManager.ResetTiles();
    }

    private void StandardGameInitialization()
    {
        Engine = new ChessEngine();
        boardManager.DisplayBoard(Engine.CurrentBoard);
    }

    private void GameOver()
    {
        uiManager.ShowGameOverScreen();
    }

    public void Rematch()
    {
        StandardGameInitialization();
        uiManager.HideGameOverScreen();
    }

    // DEBUG
    private IEnumerator DebugRoutine()
    {
        while (true)
        {
            if (Input.GetMouseButtonDown(1))
            {
                Ray ray = _mainCamera.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out RaycastHit hitInfo, Mathf.Infinity, PieceLayerMask))
                {
                    Debug.Log(hitInfo.collider.gameObject.GetComponent<Piece3D>().Coord);
                }
            }

            if (Input.GetKeyDown(KeyCode.F))
            {
                Debug.Log(FenUtils.CreateFen(Engine));
            }

            yield return null;
        }
    }
}