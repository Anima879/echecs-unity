using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Move
{
    public BoardCoord StartPosition { get; private set; }
    public BoardCoord EndPosition { get; private set; }

    public Move(BoardCoord start, BoardCoord end)
    {
        StartPosition = start;
        EndPosition = end;
    }
}
