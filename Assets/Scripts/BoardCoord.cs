﻿using System;
using System.Collections.Generic;
using UnityEngine;

// TODO : refactor struct
public readonly struct BoardCoord
{
    private readonly char _file;
    private readonly int _range;
    private readonly float _x;
    private readonly float _y;
    private readonly int _xIndex;
    private readonly int _yIndex;

    public Vector3 Vector => new Vector3(GetX(), 0, GetY());

    public BoardCoord(char file, int range)
    {

        if (range < 1 || range > 8)
        {
            throw new Exception("Range must be between 1 and 8");
        }
            
        _file = file;
        _range = range;

        switch (_file)
        {
            case 'a':
                _x = -3.5f;
                _xIndex = 0;
                break;
            case 'b':
                _x = -2.5f;
                _xIndex = 1;
                break;
            case 'c':
                _x = -1.5f;
                _xIndex = 2;
                break;
            case 'd':
                _x = -.5f;
                _xIndex = 3;
                break;
            case 'e':
                _x = .5f;
                _xIndex = 4;
                break;
            case 'f':
                _x = 1.5f;
                _xIndex = 5;
                break;
            case 'g':
                _x = 2.5f;
                _xIndex = 6;
                break;
            case 'h':
                _x = 3.5f;
                _xIndex = 7;
                break;
            default:
                throw new Exception("Illegal argument");
        }

        _y = _range - 4.5f;
        _yIndex = _range - 1;
    }

    public BoardCoord(int x, int y) : this(IndexToColumn(x), y + 1)
    {
    }

    public static IEnumerable<BoardCoord> EnumerateBoardCoords()
    {
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                yield return new BoardCoord(i, j);
            }
        }
    }

    public float GetX()
    {
        return _x;
    }

    public float GetY()
    {
        return _y;
    }

    public int GetXIndex()
    {
        return _xIndex;
    }

    public int GetYIndex()
    {
        return _yIndex;
    }

    public int GetRange()
    {
        return _range;
    }

    public char GetColumn()
    {
        return _file;
    }

    public static char IndexToColumn(int index)
    {
        switch (index)
        {
            case 0:
                return 'a';
            case 1:
                return 'b';
            case 2:
                return 'c';
            case 3:
                return 'd';
            case 4:
                return 'e';
            case 5:
                return 'f';
            case 6:
                return 'g';
            case 7:
                return 'h';
            default:
                throw new Exception("Illegal argument");
        }
    }
        
    public static int ColumnToIndex(char column)
    {
        switch (column)
        {
            case 'a':
                return 0;
            case 'b':
                return 1;
            case 'c':
                return 2;
            case 'd':
                return 3;
            case 'e':
                return 4;
            case 'f':
                return 5;
            case 'g':
                return 6;
            case 'h':
                return 7;
            default:
                throw new Exception("Illegal argument");
        }
    }

    public override string ToString()
    {
        return _file + "" + _range;
    }

    public bool Equals(BoardCoord other)
    {
        return _xIndex == other._xIndex && _yIndex == other._yIndex;
    }

    public override bool Equals(object obj)
    {
        return obj is BoardCoord other && Equals(other);
    }

    public override int GetHashCode()
    {
        unchecked
        {
            return (_xIndex * 397) ^ _yIndex;
        }
    }
}