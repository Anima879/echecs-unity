﻿using System;
using System.Collections.Generic;
using Assets.Scripts;
using Player;

public class Piece
{
    public PieceType Type { get; set; }
    // public BoardCoord Coord { get; set; }
    // public bool IsFirstMove { get; set; }
    public Side Side { get; set; }
    // TODO : use other data struct
    public int Value { get; set; }
    public bool IsAlive { get; set; }

    public Piece(Side side, PieceType type)
    {
        IsAlive = true;
        Side = side;
        Type = type;
    }
        
    private static IEnumerable<BoardCoord> NextCardinal(BoardCoord coord, char direction)
    {
        switch (direction)
        {
            case 'E':
                for (int i = coord.GetXIndex() + 1; i < 8; i++)
                {
                    yield return new BoardCoord(i, coord.GetYIndex());
                }

                break;
            case 'W':
                for (int i = coord.GetXIndex() - 1; i >= 0; i--)
                {
                    yield return new BoardCoord(i, coord.GetYIndex());
                }

                break;
            case 'N':
                for (int i = coord.GetYIndex() + 1; i < 8; i++)
                {
                    yield return new BoardCoord(coord.GetXIndex(), i);
                }

                break;
            case 'S':
                for (int i = coord.GetYIndex() - 1; i >= 0; i--)
                {
                    yield return new BoardCoord(coord.GetXIndex(), i);
                }

                break;
            default:
                throw new Exception("Direction must be 'W', 'E', 'N' or 'S'");
        }
    }

    private static IEnumerable<BoardCoord> NextDiagonal(BoardCoord coord, string direction)
    {
        switch (direction)
        {
            case "NE":
                while (true)
                {
                    try
                    {
                        coord = new BoardCoord(coord.GetXIndex() + 1, coord.GetYIndex() + 1);
                    }
                    catch
                    {
                        break;
                    }

                    yield return coord;
                }

                break;
            case "NW":
                while (true)
                {
                    try
                    {
                        coord = new BoardCoord(coord.GetXIndex() - 1, coord.GetYIndex() + 1);
                    }
                    catch
                    {
                        break;
                    }

                    yield return coord;
                }

                break;
            case "SE":
                while (true)
                {
                    try
                    {
                        coord = new BoardCoord(coord.GetXIndex() + 1, coord.GetYIndex() - 1);
                    }
                    catch
                    {
                        break;
                    }

                    yield return coord;
                }

                break;
            case "SW":
                while (true)
                {
                    try
                    {
                        coord = new BoardCoord(coord.GetXIndex() - 1, coord.GetYIndex() - 1);
                    }
                    catch
                    {
                        break;
                    }

                    yield return coord;
                }

                break;
            default:
                throw new Exception("Direction must be 'NW', 'NE', 'SE' or 'SW'");
        }
    }

    public IEnumerable<BoardCoord> NextMovementCoord(BoardCoord origin, Piece[,] board)
    {
        switch (Type)
        {
            case PieceType.Pawn:
                foreach (BoardCoord boardCoord in NextPawnMovement(origin, board)) yield return boardCoord;
                break;
            case PieceType.Knight:
                foreach (BoardCoord boardCoord in NextKnightMovement(origin, board)) yield return boardCoord;
                break;
            case PieceType.Bishop:
                foreach (BoardCoord boardCoord in NextBishopMovement(origin, board)) yield return boardCoord;
                break;
            case PieceType.Rook:
                foreach (BoardCoord boardCoord in NextRookMovement(origin, board)) yield return boardCoord;
                break;
            case PieceType.Queen:
                foreach (BoardCoord boardCoord in NextQueenMovement(origin, board)) yield return boardCoord;
                break;
            case PieceType.King:
                foreach (BoardCoord boardCoord in NextKingMovement(origin, board)) yield return boardCoord;
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
    
    // TODO : Refactor moves generator

    private IEnumerable<BoardCoord> NextRookMovement(BoardCoord origin, Piece[,] board)
    {
        char[] directions = new[] {'N', 'S', 'E', 'W'};

        foreach (char dir in directions)
        {
            foreach (BoardCoord coord in NextCardinal(origin, dir))
            {
                Piece piece = board[coord.GetXIndex(), coord.GetYIndex()];
                if (piece != null && piece.Side == Side) break;
                if (piece != null && piece.Side != Side)
                {
                    yield return coord;
                    break;
                }
                    
                yield return coord;
            }
        }
    }

    private IEnumerable<BoardCoord> NextBishopMovement(BoardCoord origin, Piece[,] board)
    {
        string[] directions = new[] {"NW", "NE", "SE", "SW"};

        foreach (string dir in directions)
        {
            foreach (BoardCoord coord in NextDiagonal(origin, dir))
            {
                Piece piece = board[coord.GetXIndex(), coord.GetYIndex()];
                if (piece != null && piece.Side == Side) break;
                if (piece != null && piece.Side != Side)
                {
                    yield return coord;
                    break;
                }
                    
                yield return coord;
            }
        }
    }

    private IEnumerable<BoardCoord> NextQueenMovement(BoardCoord origin, Piece[,] board)
    {
        foreach (BoardCoord coord in NextRookMovement(origin, board)) yield return coord;
        foreach (BoardCoord coord in NextBishopMovement(origin, board)) yield return coord;
    }

    private IEnumerable<BoardCoord> NextKingMovement(BoardCoord origin, Piece[,] board)
    {
        List<BoardCoord> availableCoord = new List<BoardCoord>();
        for (int i = -1; i <= 1; i++)
        {
            for (int j = -1; j <= 1; j++)
            {
                int xIndex = origin.GetXIndex() + i;
                int yIndex = origin.GetYIndex() + j;

                try
                {
                    BoardCoord newCoord = new BoardCoord(xIndex, yIndex);
                    if (board[newCoord.GetXIndex(), newCoord.GetYIndex()] == null ||
                        !board[newCoord.GetXIndex(), newCoord.GetYIndex()].Side.Equals(Side))
                    {
                        availableCoord.Add(newCoord);
                    }
                }
                catch
                {
                    // ignore
                }
            }
        }

        foreach (BoardCoord c in availableCoord)
        {
            yield return c;
        }
    }

    private IEnumerable<BoardCoord> NextKnightMovement(BoardCoord origin, Piece[,] board)
    {
        int[,] dxdy = new[,] {{2, 1}, {2, -1}, {-2, 1}, {-2, -1}};
            
        List<BoardCoord> coords = new List<BoardCoord>();
        for (int i = 0; i < dxdy.GetLength(0); i++)
        {
            int xIndex = origin.GetXIndex() + dxdy[i, 0];
            int yIndex = origin.GetYIndex() + dxdy[i, 1];

            BoardCoord newCoord;
            try
            {
                newCoord = new BoardCoord(xIndex, yIndex);
                if (board[newCoord.GetXIndex(), newCoord.GetYIndex()] == null ||
                    !board[newCoord.GetXIndex(), newCoord.GetYIndex()].Side.Equals(Side))
                {
                    coords.Add(newCoord);
                }
            }
            catch
            {
                // ignore
            }

            xIndex = origin.GetXIndex() + dxdy[i, 1];
            yIndex = origin.GetYIndex() + dxdy[i, 0];

            try
            {
                newCoord = new BoardCoord(xIndex, yIndex);
                if (board[newCoord.GetXIndex(), newCoord.GetYIndex()] == null ||
                    !board[newCoord.GetXIndex(), newCoord.GetYIndex()].Side.Equals(Side))
                {
                    coords.Add(newCoord);
                }
            }
            catch
            {
                // ignore
            }
        }

        foreach (BoardCoord c in coords)
        {
            yield return c;
        }
    }

    private IEnumerable<BoardCoord> NextPawnMovement(BoardCoord origin, Piece[,] board)
    {
        if (Side.Equals(Side.White) && origin.GetRange() == 8) yield break;
        if (Side.Equals(Side.Black) && origin.GetRange() == 1) yield break;

        int direction = Side.Equals(Side.White) ? 1 : -1;
        BoardCoord tileCoord = new BoardCoord(origin.GetXIndex(), origin.GetYIndex() + 1 * direction);
        Piece piece = board[tileCoord.GetXIndex(), tileCoord.GetYIndex()];
        if (piece == null) yield return tileCoord;
            
        // TODO : refactor
        if (origin.GetRange() == 1 || origin.GetRange() == 7)
        {
            tileCoord = new BoardCoord(origin.GetXIndex(), origin.GetYIndex() + 2 * direction);
            piece = board[tileCoord.GetXIndex(), tileCoord.GetYIndex()];
            if (piece == null) yield return tileCoord;
        }

        // Check if pawn can eat an enemy piece
        if (origin.GetXIndex() < 7)
        {
            tileCoord = new BoardCoord(origin.GetXIndex() + 1, origin.GetYIndex() + 1 * direction);
            piece = board[tileCoord.GetXIndex(), tileCoord.GetYIndex()];
            if (piece != null && !piece.Side.Equals(Side)) yield return tileCoord;
                
        }

        if (origin.GetXIndex() > 0)
        {
            tileCoord = new BoardCoord(origin.GetXIndex() - 1, origin.GetYIndex() + 1 * direction);
            piece = board[tileCoord.GetXIndex(), tileCoord.GetYIndex()];
            if (piece != null && !piece.Side.Equals(Side)) yield return tileCoord;
        }
    }
    
    public enum PieceType
    {
        Pawn,
        Knight,
        Bishop,
        Rook,
        Queen,
        King
    }
}