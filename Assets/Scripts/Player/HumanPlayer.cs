﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;

namespace Player
{
    public class HumanPlayer : AbstractPlayer
    {
        private static readonly LayerMask PieceLayerMask = 1 << 9;
        private static readonly LayerMask TileLayerMask = 1 << 8;

        private Camera _camera;

        protected override void Awake()
        {
            base.Awake();
            selectedPiece = null;
            legalMoves = new List<BoardCoord>();
        }

        protected override void Start()
        {
            base.Start();
            _camera = Camera.main;
        }

        protected override IEnumerator Playing(State state)
        {
            while (PlayerState == state)
            {
                Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
                HoverPiece(ray);
                Play(ray);

                yield return null;
            }

            selectedPiece = null;
        }

        protected override IEnumerator Checkmate(State state)
        {
            while (PlayerState == state)
            {
                yield return null;
            }
        }

        private void Play(Ray ray)
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (selectedPiece == null)
                {
                    if (Physics.Raycast(ray, out RaycastHit hitInfo, Mathf.Infinity, PieceLayerMask))
                    {
                        Piece3D piece = hitInfo.collider.GetComponent<Piece3D>();
                        SelectPiece(piece);
                    }
                }
                else
                {
                    if (Physics.Raycast(ray, out RaycastHit hitInfo, Mathf.Infinity, PieceLayerMask))
                    {
                        Piece3D piece = hitInfo.collider.GetComponent<Piece3D>();
                        if (piece.side != PlayerSide)
                        {
                            PlayMove(selectedPiece, piece.Coord);
                            DeselectPiece();
                        }
                        else
                        {
                            DeselectPiece();
                            SelectPiece(piece);
                        }
                    }
                    else if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity, TileLayerMask))
                    {
                        Tile tile = hitInfo.collider.GetComponent<Tile>();
                        PlayMove(selectedPiece, tile.TileCoord);
                        DeselectPiece();
                    }
                }
            }
        }

        protected override void PlayMove(Piece3D pieceToMove, BoardCoord destination)
        {
            if (pieceToMove == null || legalMoves.Count == 0) return;
            foreach (BoardCoord coord in legalMoves)
            {
                if (coord.Equals(destination))
                {
                    if (gameMaster.PlayMove(pieceToMove.Coord, coord)) pieceToMove.Coord = destination;
                    PlayerState = State.Waiting;
                }
            }
        }

        private void SelectPiece(Piece3D piece)
        {
            if (piece.side != PlayerSide) return;
            DeselectPiece();
            selectedPiece = piece;
            legalMoves = gameMaster.ShowAvailableMoves(piece);
            piece.mouseEvent = false;
        }

        private void HoverPiece(Ray ray)
        {
            if (Physics.Raycast(ray, out RaycastHit hitInfo, Mathf.Infinity, PieceLayerMask))
            {
                Piece3D piece = hitInfo.collider.GetComponent<Piece3D>();
                if (piece.side == PlayerSide)
                {
                    piece.ShowSelectionOutline();
                }
            }
        }

        private void DeselectPiece()
        {
            if (selectedPiece != null)
            {
                selectedPiece.mouseEvent = true;
                selectedPiece.HideOutline();
                selectedPiece = null;
                gameMaster.ResetBoardTiles();
            }
        }
    }
}