﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;

namespace Player.IA
{
    public class IAPlayer : AbstractPlayer
    {
        private Stockfish _stockfish;
        private int _timeToThinkMs;

        protected override void Awake()
        {
            base.Awake();
            
            selectedPiece = null;
            legalMoves = new List<BoardCoord>();
            _stockfish = new Stockfish();
            _stockfish.LimitStrength(true);
            _stockfish.SetSkillLevel(0);
            _timeToThinkMs = 1000;
        }

        private string GetBestMove(int timeToThink)
        {
            var fen = FenUtils.CreateFen(gameMaster.Engine);
            var bestMove = _stockfish.GetMove(fen, timeToThink);
            Debug.Log(bestMove);
            bestMove = bestMove.Substring(9);
            return bestMove.Substring(0, 4);
        }

        private void ReadAndPlayMove(string move)
        {
            var startCoordString = move.Substring(0, 2);
            var endCoordString = move.Substring(2, 2);
            
            var startCoord = new BoardCoord(startCoordString[0], int.Parse(startCoordString[1].ToString()));
            var endCoord = new BoardCoord(endCoordString[0], int.Parse(endCoordString[1].ToString()));
            var piece = gameMaster.boardManager.GetPiece(startCoord);
            
            PlayMove(piece, endCoord);
        }

        protected override void PlayMove(Piece3D pieceToMove, BoardCoord destination)
        {
            if (pieceToMove == null) return;
            gameMaster.PlayMove(pieceToMove.Coord, destination);
            StartCoroutine(WaitForPiece(pieceToMove));
        }

        protected override IEnumerator Playing(State state)
        {
            while (PlayerState == state)
            {
                yield return new WaitForSeconds(_timeToThinkMs / 1000.0f);
                ReadAndPlayMove(GetBestMove(1000));
                PlayerState = State.Waiting;
                yield return null;
            }
        }

        protected override IEnumerator Checkmate(State state)
        {
            throw new System.NotImplementedException();
        }
    }
}