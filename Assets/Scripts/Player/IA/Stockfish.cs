﻿using System;
using System.Diagnostics;
using System.IO;
using Debug = UnityEngine.Debug;

namespace Player.IA
{
    public class Stockfish
    {
        private static readonly string PathStockfishEngine =
            @"D:\Projet Dev\Stockfish\stockfish-11-win\Windows\stockfish_20011801_x64.exe";

        private Process _pProcess;
        private StreamWriter _writer;
        private StreamReader _reader;
        
        public Stockfish()
        {
            _pProcess = new Process
            {
                StartInfo =
                {
                    FileName = PathStockfishEngine,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardInput = true,
                    WindowStyle = ProcessWindowStyle.Hidden,
                    CreateNoWindow = true
                }
            };
            _pProcess.Start();

            _writer = _pProcess.StandardInput;
            _reader = _pProcess.StandardOutput;

            _reader.ReadLine();
            _writer.WriteLine("isready");
            string output = _reader.ReadLine();
            if (output != "readyok")
            {
                throw new Exception("Engine initialization failed");
            }
            Debug.Log(output);
        }

        ~Stockfish()
        {
            _writer.WriteLine("quit");
            _writer.Close();
            _reader.Close();
            _pProcess.Dispose();
            _pProcess.Close();
        }

        public void LimitStrength(bool limit)
        {
            _writer.WriteLine("setoption name UCI_LimitStrength value " + limit.ToString());
        }

        public void SetSkillLevel(int level)
        {
            if (level < 0 || level > 20)
            {
                throw new Exception("Level must be between 0 and 20");
            }
            
            _writer.WriteLine("setoption name Skill Level value " + level.ToString());
            Debug.Log("setoption name Skill Level value " + level.ToString());
        }

        public void SetStartingPosition()
        {
            _writer.WriteLine("position startpos");
        }

        public void SetPosition(string fen)
        {
            _writer.WriteLine("position fen " + fen);
        }

        public string GetPosition()
        {
            _writer.WriteLine("d");
            string output = "";
            bool done = false;

            while (!done)
            {
                output = _reader.ReadLine();
                if (output != null && output.Contains("Fen")) done = true;
            }

            return output.Substring(5);
        }

        public string GetMove(string position, int moveTimes)
        {
            SetPosition(position);

            _writer.WriteLine("go movetimes " + moveTimes.ToString());

            bool done = false;
            string output = "";
            while (!done)
            {
                output = _reader.ReadLine();
                if (output != null && output.Contains("bestmove")) done = true;
            }
            
            return output;
        }

        public string GetMove(int moveTimes)
        {
            return GetMove(GetPosition(), moveTimes);
        }
    }
}