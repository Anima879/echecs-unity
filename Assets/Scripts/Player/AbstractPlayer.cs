﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
    public abstract class AbstractPlayer : MonoBehaviour
    {
        protected GameMaster gameMaster;
        public Side PlayerSide { get; set; }
        protected Piece3D selectedPiece;
        protected List<BoardCoord> legalMoves;

        public bool isCheck;
        public State PlayerState { get; set; }

        private IEnumerator _finiteStateMachineRoutine;
        private IEnumerator _stateRoutine;

        public enum State
        {
            Start,
            Playing,
            Waiting,
            Checkmate,
            Winner,
            Exit
        }

        protected abstract void PlayMove(Piece3D pieceToMove, BoardCoord destination);

        protected virtual void Awake()
        {
            _finiteStateMachineRoutine = FiniteStateMachine(State.Start);
        }

        protected virtual void Start()
        {
            StartCoroutine(_finiteStateMachineRoutine);
        }

        #region FiniteStateMachine
        
        private IEnumerator FiniteStateMachine(State state)
        {
            PlayerState = state;
            while (PlayerState != State.Exit)
            {
                switch (PlayerState)
                {
                    case State.Start:
                        _stateRoutine = Start(PlayerState);
                        break;
                    case State.Playing:
                        _stateRoutine = Playing(PlayerState);
                        break;
                    case State.Waiting:
                        _stateRoutine = Waiting(PlayerState);
                        break;
                    case State.Checkmate:
                        Debug.Log("State : " + PlayerState);
                        break;
                    case State.Exit:
                        Debug.Log("State : " + PlayerState);
                        break;
                    default:
                        throw new Exception("State not implemented yet");
                }
                
                yield return StartCoroutine(_stateRoutine);
            }
        }

        protected IEnumerator Start(State state)
        {
            Debug.Log("Enter Start state");
            while (PlayerState == state)
            {
                PlayerState = PlayerSide == Side.White ? State.Playing : State.Waiting;
                yield return null;
            }
        }

        protected abstract IEnumerator Playing(State state);

        protected virtual IEnumerator Waiting(State state)
        {
            while (PlayerState == state)
            {
                // Just wait and do nothing.
                yield return null;
            }
        }

        protected abstract IEnumerator Checkmate(State state);

        #endregion

        protected IEnumerator WaitForPiece(Piece3D movingPiece)
        {
            while (movingPiece.isMoving)
            {
                yield return null;
            }
        }

        public void SetGameMaster(GameMaster master)
        {
            gameMaster = master;
        }
    }

    public enum Side
    {
        White,
        Black
    }
}