using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/PiecesPrefab")]
public class PiecesPrefabsScriptableObject : ScriptableObject
{
    public GameObject pawnPrefab;
    public GameObject bishopPrefab;
    public GameObject knightPrefab;
    public GameObject rookPrefab;
    public GameObject queenPrefab;
    public GameObject kingPrefab;
    public Material whiteMaterial;
    public Material blackMaterial;
}
