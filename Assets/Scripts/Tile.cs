﻿using OutlineEffect;
using UnityEngine;

namespace Assets.Scripts
{
    [RequireComponent(typeof(Outline))]
    public class Tile : MonoBehaviour
    {
        private static int MOVE_OUTLINE = 1;
        private static int CHECK_OUTLINE = 0;
        
        public bool IsWhite;
        public Material WhiteMaterial;
        public Material BlackMaterial;
        public BoardCoord TileCoord;

        private Renderer _rend;
        private Outline _outliner;

        void Awake()
        {
            _rend = gameObject.GetComponent<Renderer>();
            _outliner = GetComponent<Outline>();
        }

        // Update is called once per frame
        void Start()
        {
            SetDefaultMaterial();
            HideOutline();
        }

        void Update()
        {
            
        }

        public void SetDefaultMaterial()
        {
            _rend.material = IsWhite ? WhiteMaterial : BlackMaterial;
        }

        public void HideOutline()
        {
            _outliner.eraseRenderer = true;
        }

        public void ShowMoveOutline()
        {
            _outliner.eraseRenderer = false;
            _outliner.color = MOVE_OUTLINE;
        }

        public void ShowCheckOutline()
        {
            _outliner.eraseRenderer = false;
            _outliner.color = CHECK_OUTLINE;
        }

        void SetColor(bool isWhite)
        {
            IsWhite = isWhite;
        }
    }
}