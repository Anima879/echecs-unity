﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using Assets.Scripts;
using OutlineEffect;
using Player;
using UnityEngine;

public class Board : MonoBehaviour
{
    private static int SIZE = 8;
    private static readonly LayerMask PieceLayerMask = 1 << 9;

    public GameObject tilePrefab;
    public PiecesPrefabsScriptableObject piecesPrefabs;

    private Piece3D _whiteKing;
    private Piece3D _blackKing;

    private List<Piece3D> _pieces;
    private Tile[] _tiles;
    public Side playerTurn;

    private AbstractPlayer _whites, _blacks;

    private Ray _screenMouse;
    private Camera _mainCamera;

    void Awake()
    {
        _pieces = new List<Piece3D>();
        _tiles = new Tile[SIZE * SIZE];
    }

    void Start()
    {
        _mainCamera = Camera.main;
        DrawBoard();
    }

    // Update is called once per frame
    void Update()
    {
        _screenMouse = _mainCamera.ScreenPointToRay(Input.mousePosition);
    }

    private void DrawBoard()
    {
        bool isWhite = false;

        int tileIndex = 0;
        for (int y = -SIZE / 2; y < SIZE / 2; y++)
        {
            for (int x = -SIZE / 2; x < SIZE / 2; x++)
            {
                GameObject tile = Instantiate(tilePrefab, new Vector3(x + .5f, 0, y + .5f), Quaternion.identity);
                tile.GetComponent<Tile>().IsWhite = isWhite;
                tile.GetComponent<Tile>().TileCoord = new BoardCoord(x + SIZE / 2, y + SIZE / 2);
                tile.transform.parent = transform;
                _tiles[tileIndex] = tile.GetComponent<Tile>();
                isWhite = !isWhite;
                tileIndex++;
            }

            isWhite = !isWhite;
        }
    }

    private Tile FetchTile(BoardCoord coord)
    {
        foreach (Tile tile in _tiles)
        {
            if (tile.TileCoord.Equals(coord))
            {
                return tile;
            }
        }

        throw new Exception("Tile not found at " + coord);
    }

    public Piece3D GetPiece(BoardCoord coord)
    {
        foreach (Piece3D piece3D in _pieces)
        {
            if (piece3D.Coord.Equals(coord))
            {
                return piece3D;
            }
        }

        Debug.LogError("3D Piece not found at " + coord);
        return null;
    }

    public void MovePiece(BoardCoord start, BoardCoord destination)
    {
        Piece3D piece = GetPiece(start);
        if (piece == null) throw new Exception("No piece found at " + start);
        piece.Move(destination.Vector);
    }

    public void AssignPlayer(AbstractPlayer whites, AbstractPlayer blacks)
    {
        _whites = whites;
        _blacks = blacks;
    }

    public void ShowMovementTiles(IEnumerable<BoardCoord> availableCoords)
    {
        foreach (BoardCoord coord in availableCoords)
        {
            FetchTile(coord).ShowMoveOutline();
        }
    }

    public void ResetTiles()
    {
        foreach (Tile tile in _tiles)
        {
            tile.HideOutline();
        }
    }

    public void SelectPiece(Piece3D piece)
    {
        piece.mouseEvent = false;
        piece.ShowSelectionOutline();
    }

    public void DeselectPiece(Piece3D piece)
    {
        piece.mouseEvent = true;
        piece.HideOutline();
    }

    public void Promote(BoardCoord coord, Side side)
    {
        AbstractPlayer player = side == Side.White ? _whites : _blacks;
        SpawnPiece(new Piece(side, Piece.PieceType.Queen), player, coord);
    }

    public void EmptyGrid()
    {
        foreach (Piece3D piece in _pieces)
        {
            Destroy(piece.gameObject);
        }

        _pieces = new List<Piece3D>();
        _whiteKing = null;
        _blackKing = null;
    }

    public void DisplayBoard(BoardModel board)
    {
        EmptyGrid();

        foreach (BoardCoord coord in BoardCoord.EnumerateBoardCoords())
        {
            Piece piece = board.GetPiece(coord);
            if (piece == null) continue;
            AbstractPlayer player = piece.Side.Equals(Side.White) ? _whites : _blacks;
            SpawnPiece(piece, player, coord);
        }
    }

    private void SpawnPiece(Piece piece, AbstractPlayer owner, BoardCoord coord)
    {
        GameObject newPiece;
        switch (piece.Type)
        {
            case Piece.PieceType.Pawn:
                newPiece = Instantiate(piecesPrefabs.pawnPrefab, new Vector3(coord.GetX(), .05f, coord.GetY()),
                    Quaternion.Euler(new Vector3(-90, 0, 0)));
                break;
            case Piece.PieceType.Bishop:
                newPiece = Instantiate(piecesPrefabs.bishopPrefab, new Vector3(coord.GetX(), .05f, coord.GetY()),
                    Quaternion.Euler(new Vector3(-90, 0, 0)));
                break;
            case Piece.PieceType.King:
                if (owner.PlayerSide == Side.White && _whiteKing != null ||
                    owner.PlayerSide == Side.Black && _blackKing != null)
                {
                    Debug.LogError("One king allowed");
                    return;
                }

                newPiece = Instantiate(piecesPrefabs.kingPrefab, new Vector3(coord.GetX(), .05f, coord.GetY()),
                    Quaternion.Euler(new Vector3(-90, 0, 0)));
                if (owner.PlayerSide == Side.White)
                {
                    _whiteKing = newPiece.GetComponent<Piece3D>();
                }
                else
                {
                    _blackKing = newPiece.GetComponent<Piece3D>();
                }

                break;
            case Piece.PieceType.Rook:
                newPiece = Instantiate(piecesPrefabs.rookPrefab, new Vector3(coord.GetX(), .05f, coord.GetY()),
                    Quaternion.Euler(new Vector3(-90, 0, 0)));
                break;
            case Piece.PieceType.Queen:
                newPiece = Instantiate(piecesPrefabs.queenPrefab, new Vector3(coord.GetX(), .05f, coord.GetY()),
                    Quaternion.Euler(new Vector3(-90, 0, 0)));
                break;
            case Piece.PieceType.Knight:
                Quaternion rotation = owner.PlayerSide == Side.White
                    ? Quaternion.Euler(new Vector3(-90, 180, 0))
                    : Quaternion.Euler(new Vector3(-90, 0, 0));
                newPiece = Instantiate(piecesPrefabs.knightPrefab, new Vector3(coord.GetX(), .05f, coord.GetY()),
                    rotation);
                break;
            default:
                throw new Exception("Illegal piece type");
        }

        newPiece.transform.localScale = Vector3.one * 30;
        newPiece.transform.parent = owner.gameObject.transform;
        newPiece.GetComponent<Outline>().eraseRenderer = true;

        Piece3D piece3D = newPiece.GetComponent<Piece3D>();
        piece3D.Coord = coord;
        piece3D.side = owner.PlayerSide;

        piece3D.GetComponent<Renderer>().material = owner.PlayerSide == Side.White
            ? piecesPrefabs.whiteMaterial
            : piecesPrefabs.blackMaterial;
        _pieces.Add(piece3D);
    }

    #region Debug

    void DisplayGridDebug()
    {
        Debug.Log("------- Grid ------------");
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                Debug.Log(i + " " + j + " "); // TODO : display grid
            }
        }

        Debug.Log("-------------------------");
    }

    #endregion
}