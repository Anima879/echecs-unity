﻿using System;
using System.Collections.Generic;
using System.Linq;
using Player;

public static class FenUtils
{
    public static readonly string StartPos = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";

    public static string CreateFen(ChessEngine chess)
    {
        string fen = "";
        
        string position = "";
        Piece[,] board = chess.CurrentBoard.Board;
        for (int j = 7; j >= 0; j--)
        {
            if (j != 7) position += "/";

            int empty = 0;
            for (int i = 0; i < 8; i++)
            {
                if (board[i, j] == null)
                {
                    empty++;
                    continue;
                }

                if (empty != 0)
                {
                    position += empty.ToString();
                    empty = 0;
                }
                position += PieceToChar(board[i, j]).ToString();
            }

            if (empty != 0) position += empty.ToString();
        }

        fen += position + " ";
        fen += chess.GetPlayerTurn().Equals(Side.White) ? "w " : "b ";
        fen += GetCastlingFen(chess) + " ";
        fen += "- "; // TODO : En passant
        fen += "0 "; // TODO : Halfmove clock
        fen += chess.GetNumberOfMove().ToString();

        return fen;
    }

    private static string GetCastlingFen(ChessEngine chess)
    {
        string whiteCastling = "";
        if (chess.CanCastleKingSide(chess.CurrentBoard, Side.White)) whiteCastling += "K";
        if (chess.CanCastleQueenSide(chess.CurrentBoard, Side.White)) whiteCastling += "Q";
        string blackCastling = "";
        if (chess.CanCastleKingSide(chess.CurrentBoard, Side.Black)) blackCastling += "k";
        if (chess.CanCastleQueenSide(chess.CurrentBoard, Side.Black)) blackCastling += "q";

        return whiteCastling == "" && blackCastling == "" ? "-" : whiteCastling + blackCastling;
    }

    public static Side ReadPlayerTurn(string fen)
    {
        string playerChar = fen.Split(' ')[1];
        return playerChar == "w" ? Side.White : Side.Black;
    }

    public static Piece[,] ReadPosition(string fen)
    {
        Piece[,] board = new Piece[8, 8];
        string position = fen.Split(' ')[0];
        string[] rows = position.Split('/');
        rows = rows.Reverse().ToArray();
        for (int i = 7; i >= 0; i--)
        {
            Piece[] row = ReadRow(rows[i]);

            for (int j = 0; j < 8; j++)
            {
                Piece piece = row[j];
                board[j, i] = piece;
            }
        }

        return board;
    }

    public static int ReadNumberOfMove(string fen)
    {
        return int.Parse(fen.Split(' ')[5]);
    }

    public static bool CanCastle(string fen, Side player, char side)
    {
        string castle = fen.Split(' ')[2];
        if (player.Equals(Side.White))
        {
            if (side == 'k')
            {
                return castle.Contains("K");
            }
            else if (side == 'q')
            {
                return castle.Contains("Q");
            }
            else
            {
                throw new Exception("side must be 'k' or 'q'");
            }
        }
        else
        {
            if (side == 'k')
            {
                return castle.Contains("k");
            }
            else if (side == 'q')
            {
                return castle.Contains("q");
            }
            else
            {
                throw new Exception("side must be 'k' or 'q'");
            }
        }
    }

    private static Piece[] ReadRow(string row)
    {
        Piece[] rowPiece = new Piece[8];
        int index = 0;
        foreach (char letter in row)
        {
            if (int.TryParse(letter.ToString(), out int empty))
            {
                for (int i = 0; i < empty; i++) rowPiece[index] = null;
            }
            else
            {
                rowPiece[index] = CharToPiece(letter);
            }

            index++;
        }

        return rowPiece;
    }

    private static Piece CharToPiece(char letter)
    {
        Side side = char.IsUpper(letter) ? Side.White : Side.Black;
        Piece piece;
        switch (char.ToLower(letter))
        {
            case 'r':
                piece = new Piece(side, Piece.PieceType.Rook);
                break;
            case 'n':
                piece = new Piece(side, Piece.PieceType.Knight);
                break;
            case 'b':
                piece = new Piece(side, Piece.PieceType.Bishop);
                break;
            case 'q':
                piece = new Piece(side, Piece.PieceType.Queen);
                break;
            case 'k':
                piece = new Piece(side, Piece.PieceType.King);
                break;
            case 'p':
                piece = new Piece(side, Piece.PieceType.Pawn);
                break;
            default:
                piece = null;
                break;
        }

        return piece;
    }

    private static char PieceToChar(Piece piece)
    {
        char type;
        switch (piece.Type)
        {
            case Piece.PieceType.Bishop:
                type = 'b';
                break;
            case Piece.PieceType.Pawn:
                type = 'p';
                break;
            case Piece.PieceType.King:
                type = 'k';
                break;
            case Piece.PieceType.Knight:
                type = 'n';
                break;
            case Piece.PieceType.Queen:
                type = 'q';
                break;
            case Piece.PieceType.Rook:
                type = 'r';
                break;
            default:
                throw new Exception();
        }

        return piece.Side.Equals(Side.White) ? char.ToUpper(type) : type;
    }
}