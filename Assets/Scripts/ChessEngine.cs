using System;
using System.Collections.Generic;
using Player;
using UnityEngine;

/// <summary>
/// Chess Engine class. Contains all the attributes and the methods for the representation of the chess game.
/// </summary>
public class ChessEngine
{
    public BoardModel CurrentBoard => _board;

    private BoardModel _board;
    private int _numberOfMove;
    private List<BoardModel> _historic;
    private Side _playerTurn;

    private bool _whiteCanCastle;
    private bool _blackCanCastle;

    private bool _waitForPromotion;
    public delegate void PieceDeathDelegate(Piece piece);

    public delegate void PromotionDelegate(BoardCoord coord, Side side);

    public delegate void CheckmateDelegate();

    public event PieceDeathDelegate OnPieceDeath;
    public event PromotionDelegate OnPromotion;
    public event CheckmateDelegate OnCheckmate;

    /// <summary>
    /// Basic constructor with empty board.
    /// </summary>
    public ChessEngine()
    {
        _playerTurn = Side.White;
        _historic = new List<BoardModel>();
        _numberOfMove = 1;
        _board = new BoardModel();
        _board.SetStartingPosition();
        _whiteCanCastle = true;
        _blackCanCastle = true;
        _waitForPromotion = false;
    }

    // TODO : remove getter
    public Side GetPlayerTurn()
    {
        return _playerTurn;
    }

    // TODO : Remove getter
    public int GetNumberOfMove()
    {
        return _numberOfMove;
    }

    /// <summary>
    /// Play a move given two coordinates of the board.
    /// </summary>
    /// <param name="start">The starting coordinate</param>
    /// <param name="end">The ending coordinate</param>
    /// <returns>True if the move was correctly played, false if the move was illegal</returns>
    public bool PlayMove(BoardCoord start, BoardCoord end)
    {
        if (_waitForPromotion)
        {
            Debug.LogError("Cannot make move, waiting for promotion");
            return false;
        }
        
        Piece pieceToMove = _board.GetPiece(start);
        if (pieceToMove == null)
        {
            Debug.LogError("No piece at the starting coordinate: " + start);
            return false;
        }

        if (pieceToMove.Side != _playerTurn)
        {
            Debug.LogError(_playerTurn + "to move");
            return false;
        }

        List<BoardCoord> legalMoves = GetLegalMoves(pieceToMove, start);
        if (!legalMoves.Contains(end))
        {
            Debug.LogError("Target's coordinate is not legal");
            return false;
        }
        
        BoardModel newBoard = MovePiece(start, end, _board);
        _historic.Add(_board);
        _board = newBoard;

        // Check for promotion if a pawn moved
        if (pieceToMove.Type == Piece.PieceType.Pawn)
        {
            bool white = _playerTurn == Side.White && end.GetRange() == 8;
            bool black = _playerTurn == Side.Black && end.GetRange() == 1;
            if (white || black)
            {
                _waitForPromotion = true;
                OnPromotion?.Invoke(end, _playerTurn);
                return true;
            }
        }

        // increment number of move
        if (_playerTurn.Equals(Side.Black)) _numberOfMove++;
        // Change player
        TogglePlayer();
        // Verify if this player is checkmate by the last move
        IsCheckmate(_playerTurn, _board);
        // TODO : Verify stalemate (pat)

        return true;
    }

    /// <summary>
    /// Move the piece at the destination coordinate in the given board.
    /// </summary>
    /// <remarks>
    /// Move the piece whenever the move is legal or not.
    /// </remarks>
    /// <param name="start"> The starting coordiante</param>
    /// <param name="destination"> The destination coordinate</param>
    /// <param name="board"> The board where the move happens</param>
    /// <returns>The new board</returns>
    private BoardModel MovePiece(BoardCoord start, BoardCoord destination, BoardModel board)
    {
        Piece movingPiece = board.GetPiece(start);
        if (movingPiece == null) throw new Exception("No piece found at " + start);
        Piece targetPiece = board.GetPiece(destination);
        BoardModel nextBoard = board.DeepCopy();

        if (targetPiece != null)
        {
            targetPiece.IsAlive = false;
            OnPieceDeath?.Invoke(targetPiece);
        }

        nextBoard.SetPiece(movingPiece, destination);
        nextBoard.SetPiece(null, start);

        return nextBoard;
    }

    public void MakePromotion(Piece.PieceType typeToPromote)
    {
        if (!_waitForPromotion)
        {
            Debug.LogError("No promotion to make");
            return;
        }

        int range = _playerTurn == Side.White ? 8 : 1;

        for (int i = 0; i < 8; i++)
        {
            BoardCoord coord = new BoardCoord(i, range);
            Piece piece = _board.GetPiece(coord);
            if (piece == null) continue;
            if (piece.Type != Piece.PieceType.Pawn) continue;
            _board.SetPiece(new Piece(_playerTurn, typeToPromote), coord);
            return;
        }

        throw new Exception("No pawn found for promotion");
    }

    /// <summary>
    /// Toggle the player who has to play.
    /// </summary>
    private void TogglePlayer()
    {
        _playerTurn = _playerTurn.Equals(Side.White)
            ? Side.Black
            : Side.White;
    }

    // private string KnightMoveString(Knight knight, BoardCoord dest)
    // {
    //     string moveString = knight.GetPieceLetter();
    //
    //     foreach (Piece piece in _board.Board)
    //     {
    //         if (piece == null || !piece.Side.Equals(knight.Side)) continue;
    //         if (!piece.Type.Equals(Piece.PieceType.Knight) || piece.Equals(knight)) continue;
    //
    //         Debug.Log(piece);
    //
    //         foreach (BoardCoord coord in piece.NextMovementCoord(_board.Board))
    //         {
    //             if (coord.Equals(dest))
    //             {
    //                 if (piece.Coord.GetColumn() == knight.Coord.GetColumn())
    //                 {
    //                     return moveString + knight.Coord.GetRange().ToString();
    //                 }
    //                 else
    //                 {
    //                     return moveString + knight.Coord.GetColumn().ToString();
    //                 }
    //             }
    //         }
    //     }
    //
    //     return moveString;
    // }

    #region Moves

    private List<BoardCoord> GetLegalMoves(Piece piece, BoardCoord origin)
    {
        List<BoardCoord> legalMoves = new List<BoardCoord>();
        foreach (BoardCoord coord in piece.NextMovementCoord(origin, _board.Board))
        {
            if (IsMoveLegal(origin, coord))
            {
                legalMoves.Add(coord);
            }
        }

        if (piece.Type.Equals(Piece.PieceType.King)) legalMoves.AddRange(GetCastlingLegalMoves(piece.Side));

        return legalMoves;
    }

    public List<BoardCoord> GetLegalMoves(BoardCoord origin)
    {
        Piece piece = _board.GetPiece(origin);
        if (piece == null)
        {
            Debug.LogError("No piece found at " + origin);
            return null;
        }
        return GetLegalMoves(piece, origin);
    }

    private bool IsMoveLegal(BoardCoord start, BoardCoord destination)
    {
        BoardModel nextBoard = MovePiece(start, destination, _board);
        return !IsCheck(_playerTurn, nextBoard);
    }

    #endregion

    #region Check

    private bool IsCheck(Side side, BoardModel board)
    {
        BoardCoord kingPosition = side == Side.White ? board.WhiteKingPosition : board.BlackKingPosition;

        foreach (BoardCoord coord in BoardCoord.EnumerateBoardCoords())
        {
            Piece piece = board.GetPiece(coord);
            if (piece == null) continue;
            if (piece.Side == side) continue;

            foreach (BoardCoord attackCoord in piece.NextMovementCoord(coord, board.Board))
            {
                if (attackCoord.Equals(kingPosition)) return true;
            }
        }

        return false;
    }

    private bool IsCheckmate(Side player, BoardModel board)
    {
        foreach (BoardCoord coord in BoardCoord.EnumerateBoardCoords())
        {
            Piece piece = board.GetPiece(coord);
            if (piece == null) continue;
            if (piece.Side != player) continue;
            if (GetLegalMoves(piece, coord).Count != 0) return false;
        }

        Debug.Log(player + " is checkmate");
        OnCheckmate?.Invoke();
        return true;
    }

    #endregion

    #region Castle

    private BoardModel Castle(int side, Side player)
    {
        BoardModel nextBoard = _board.DeepCopy();
        int range = player == Side.White ? 1 : 8;
        if (side == 0)
        {
            if (!CanCastleKingSide(_board, player)) throw new Exception("Castling is illegal");
            nextBoard = MovePiece(new BoardCoord('e', range), new BoardCoord('g', range), nextBoard);
            nextBoard = MovePiece(new BoardCoord('h', range), new BoardCoord('f', range), nextBoard);
        }
        else if (side == 1)
        {
            if (!CanCastleQueenSide(_board, player)) throw new Exception("Castling is illegal");
            nextBoard = MovePiece(new BoardCoord('e', range), new BoardCoord('c', range), nextBoard);
            nextBoard = MovePiece(new BoardCoord('a', range), new BoardCoord('d', range), nextBoard);
        }
        else
        {
            throw new Exception("Side must be 0 for king side castling or 1 for queen side");
        }

        if (player == Side.White) _whiteCanCastle = false;
        if (player == Side.Black) _blackCanCastle = false;
        return nextBoard;
    }

    private List<BoardCoord> GetCastlingLegalMoves(Side side)
    {
        if (side == Side.White)
            if (!_whiteCanCastle)
                return null;
        if (side == Side.Black)
            if (!_blackCanCastle)
                return null;

        List<BoardCoord> legalMoves = new List<BoardCoord>();
        int range = side == Side.White ? 1 : 8;

        if (CanCastleKingSide(_board, side)) legalMoves.Add(new BoardCoord('g', range));
        if (CanCastleQueenSide(_board, side)) legalMoves.Add(new BoardCoord('c', range));

        return legalMoves;
    }

    public bool CanCastleKingSide(BoardModel board, Side side)
    {
        int range = side == Side.White ? 1 : 8;
        Piece king = board.GetPiece(new BoardCoord('e', range));
        if (king == null) return false;
        if (king.Type != Piece.PieceType.King) return false;
        if (IsCheck(king.Side, board)) return false;

        BoardCoord fTile = new BoardCoord('f', range);
        BoardCoord gTile = new BoardCoord('g', range);

        Side enemySide = king.Side.Equals(Side.White) ? Side.Black : Side.White;

        if (IsControlled(fTile, enemySide, board) || IsControlled(gTile, enemySide, board)) return false;
        if (board.GetPiece(fTile) != null || board.GetPiece(gTile) != null) return false;


        Piece rook = board.GetPiece(new BoardCoord('h', range));
        if (rook == null || rook.Type == Piece.PieceType.Rook) return false;

        return true;
    }

    public bool CanCastleQueenSide(BoardModel board, Side side)
    {
        int range = side == Side.White ? 1 : 8;
        Piece king = board.GetPiece(new BoardCoord('e', range));
        if (king == null) return false;
        if (king.Type != Piece.PieceType.King) return false;
        if (IsCheck(king.Side, board)) return false;

        BoardCoord bTile = new BoardCoord('b', range);
        BoardCoord cTile = new BoardCoord('c', range);
        BoardCoord dTile = new BoardCoord('d', range);

        Side enemySide = king.Side.Equals(Side.White) ? Side.Black : Side.White;
        if (IsControlled(cTile, enemySide, board) || IsControlled(dTile, enemySide, board)) return false;

        if (board.GetPiece(bTile) != null || board.GetPiece(cTile) != null || board.GetPiece(dTile) != null)
        {
            return false;
        }

        Piece rook = board.GetPiece(new BoardCoord('a', range));
        if (rook == null || rook.Type != Piece.PieceType.Rook) return false;

        return true;
    }

    private bool IsControlled(BoardCoord target, Side player, BoardModel board)
    {
        foreach (BoardCoord coord in BoardCoord.EnumerateBoardCoords())
        {
            Piece piece = board.GetPiece(coord);
            if (piece == null || piece.Side != player) continue;
            foreach (BoardCoord move in piece.NextMovementCoord(coord, board.Board))
            {
                if (coord.Equals(target)) return true;
            }
        }

        return false;
    }

    #endregion

    #region Debug

    public static void DisplayBoardDebug(Piece[,] board)
    {
        Debug.Log("------- Board ------------");
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                Debug.Log(i + " " + j + " " + board[i, j]);
            }
        }

        Debug.Log("-------------------------");
    }

    #endregion
}