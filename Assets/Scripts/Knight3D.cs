using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knight3D : Piece3D
{
    public float yMax;
    
    protected override IEnumerator MoveAnimation(Vector3 targetPosition)
    {
        Vector3 startPos = transform.position;
        float distance = Vector3.Distance(startPos, targetPosition);
        float progress = 0.0f;

        while (progress / distance < 1)
        {
            float t = progress / distance;
            progress += speed * Time.deltaTime;
            float y = 4 * t * (1 - t);
            transform.position = Vector3.Lerp(startPos, targetPosition, t) + Vector3.up * (y * yMax);
                
            yield return null;
        }

        transform.position = targetPosition;
        isMoving = false;
    }
}
